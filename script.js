const sendButton = document.querySelector('#send-button')
sendButton.addEventListener('click', sendMessage)

const ul = document.querySelector('#ul')

function sendMessage() {
  const textArea = document.querySelector('#user-message')
  const userMessage = textArea.value
  const message = document.createElement('div')
  message.classList.add('message')

  const li = document.createElement("li")

  message.innerHTML = `
            <li id="li">${userMessage}</li>
            <textarea rows="1" class="edit-textarea" hidden></textarea>
            <div class="buttons">
              <button class="edit-button" onclick="editarMensagem(this)" >Editar</button>
              <button class="delete-button" onclick="deletarMensagem(this)" >Excluir</button>
              <button class="confirm-button" onclick="confirmarMensagem(this)" hidden>Confirmar</button>
            </div>
  
  `
  ul.appendChild(message)
  textArea.value = ""
}

function deletarMensagem(deleteButton) {
  deleteButton.parentNode.parentNode.remove()
}

function editarMensagem(editButton) {
  const li = editButton.parentNode.parentNode.querySelector('#li')
  li.hidden = true

  const editMessageTextArea = editButton.parentNode.parentNode.querySelector('textarea')
  editMessageTextArea.value = li.innerText
  editMessageTextArea.hidden = false
  editMessageTextArea.focus()

  const deleteButton = editButton.parentNode.querySelector('.delete-button')
  const confirmButton = editButton.parentNode.querySelector('.confirm-button')
  deleteButton.hidden = true
  editButton.hidden = true
  confirmButton.hidden = false
}

function confirmarMensagem(confirmButton) {
  const editedMessage = confirmButton.parentNode.parentNode.querySelector('textarea').value
  const li = confirmButton.parentNode.parentNode.querySelector('#li')

  li.innerText = editedMessage

  const editMessageTextArea = confirmButton.parentNode.parentNode.querySelector('textarea')
  editMessageTextArea.hidden = true
  li.hidden = false

  const deleteButton = confirmButton.parentNode.querySelector('.delete-button')
  const editButton = confirmButton.parentNode.querySelector('.edit-button')
  deleteButton.hidden = false
  editButton.hidden = false
  confirmButton.hidden = true

}
